#include <iostream>
#include <string>
#include <valarray>

void FindSmallerNum()
{
    std::cout << "Enter the first number: " << std::endl;
    int first;
    std::cin >> first;
    
    std::cout << "Enter the second number:" << std::endl;
    int second;
    std::cin >> second;
    
    std::cout << "----- Checking-----" << std::endl;

    if(first > second)
    {
        std::cout << "Smallest number: " << second << std::endl;
    }
    else if(first == second)
    {
        std::cout << first << " equal " << second << std::endl;
    }
    else
    {
        std::cout << "Smallest number: " << first << std::endl;
    }   
}

void FindSumma()
{
    std::cout << " Enter the first number: " << std::endl;
    int first;
    std::cin >> first;
    
    std::cout << " Enter the second number:" << std::endl;
    int second;
    std::cin >> second;
    
    std::cout << " Enter their amount:" << std::endl;
    int summa;
    std::cin >> summa;
    
    std::cout << " ----- Checking----- " << std::endl;

    if( first + second == summa )
    {
        std::cout << " Right! " << std::endl;
    }
    else
    {
        std::cout << " Mistake! Correct result:  " << first + second  << std::endl;
    }
}

void FindOdd()
{
    std::cout << " Enter the first number: " << std::endl;
    int first;
    std::cin >> first;

    std::cout << " ----- Checking----- " << std::endl;
    
    bool x = first % 2 == 0;
    
    std::string Text;
    Text = x ? "Odd":"Even";
    
    std::cout << "Number - "<< first << " " << Text << std::endl;

}

void FindRemainder()
{
    std::cout << " Enter the first number: " << std::endl;
    int first;
    std::cin >> first;
    
    std::cout << " Enter the second number:" << std::endl;
    int second;
    std::cin >> second;

    std::cout << " ----- Checking----- " << std::endl;


    if(first % second == 0) std::cout << " Yes, " << first << " is divisible by " << second << " without remainder!" << std::endl;
    else std::cout << " No, " << first << " is not divisible by " << second << " without remainder! " << std::endl;
}

int FindMax(int a, int b)
{
    return a > b ? a : b;
}
int FindMin(int a, int b)
{
    return a > b ? b : a;
}

void FindMaxSalaryVar1()
{
    std::cout << " Enter the salary of the first employee: " << std::endl;
    int first;
    std::cin >> first;
    
    std::cout << " Enter the salary of the second employee: " << std::endl;
    int second;
    std::cin >> second;
    
    std::cout << " Enter the salary of the third employee: " << std::endl;
    int third;
    std::cin >> third;
    
    std::cout << " ----- We consider----- " << std::endl;

    if(first < 0 || second < 0 || third < 0)
    {
        std::cout << " Give the employee a salary " << std::endl;
        return;
    }
    
    int min = std::min(first,std::min(second,third));
    int max = std::max(first, std::min(second, third));
    
    std::cout << " The highest salary in the department: " << max << " rubles " << std::endl;
    std::cout << " The difference between the highest and lowest salary in the department: " << max - min << " rubles " << std::endl;
    std::cout << " Average salary in the department: " << (first + second + third) / 3 << " rubles " << std::endl;  
}
void FindMaxSalaryVar2()
{
    std::cout << " Enter the salary of the first employee: " << std::endl;
    int first;
    std::cin >> first;
    
    std::cout << " Enter the salary of the second employee: " << std::endl;
    int second;
    std::cin >> second;
    
    std::cout << " Enter the salary of the third employee: " << std::endl;
    int third;
    std::cin >> third;
    
    std::cout << " ----- We consider----- " << std::endl;

    if(first < 0 || second < 0 || third < 0)
    {
        std::cout << " Give the employee a salary " << std::endl;
        return;
    }
    
    int min1 = FindMin(first, second);    
    int max1 = FindMax(first, second);

    int min2 = FindMin(first, third);
    int max2 = FindMax(first, third);

    int min = FindMin(min1, min2);
    int max = FindMax(max1, max2);
    
    std::cout << " The highest salary in the department: " << max << " rubles " << std::endl;
    std::cout << " The difference between the highest and lowest salary in the department: " << max - min << " rubles " << std::endl;
    std::cout << " Average salary in the department: " << (first + second + third) / 3 << " rubles " << std::endl;  
}

void FindTax()
{
    std::cout << " Your income? Enter: " << std::endl;
    double income;
    std::cin >> income;

    if (income < 0)
    {
        std::cout << " Big brother sees everything, don't be naughty! ";
        return;
    }    
    
    double tax;
    if(income > 50000.0)
    {
        tax = (income - 50000.0)/100 * 30 + (50000.0 - 10000.0)/100 * 20 + 10000.0/100 * 13;
    }
    else if(income > 10000.0)
    {
        tax = (income - 10000.0)/100 * 20 + 10000.0/100 * 13;
    }
    else
    {
        tax = income/100 * 13;
    }

    std::cout << "Your Tax: " << tax;
}

void GetMenu()
{
    std::cout << " Enter day (1 - 7): " << std::endl;
    int day;
    std::cin >> day;

    if(day < 1 || day > 7)
    {
        std::cout << " ... (1 - 7): " << std::endl;
        return;
    }

    std::string first, second, salad, drink;
    switch (day)
    {
    case 1 :
        first = "Uha";
        second = "Pure";
        salad = "Cezar";
        drink = "Mors";
        break;
    case 2 :
        first = "Borsh";
        second = "Kotleta";
        salad = "Grecheskiy";
        drink = "Cola";
        break;
    case 3 :
        first = "Kuriniy";
        second = "GoVegan";
        salad = "Ovoshnoy";
        drink = "Voda";
        break;
    case 4 :
        first = "Uha";
        second = "GoVegan";
        salad = "Ovoshnoy";
        drink = "Mors";
        break;
    case 5 :
        first = "Kuriniy";
        second = "Kotleta";
        salad = "Grecheskiy";
        drink = "Voda";
        break;
    case 6 :
        first = "Borsh";
        second = "Pure";
        salad = "Cezar";
        drink = "Chay";
        break;
    case 7 :
        first = "Gribnoy";
        second = "Free";
        salad = "Nagetz";
        drink = "Pepsi";
        break;
    default:
        first = "Default";
        second = "Default";
        salad = "Default";
        drink = "Default";
        break;
    }   

    std::cout << " First course - " << first << " \n Second course - " << second << " \n Salad - " << salad << " \n Drink - " << drink;
}

void GetLevel()
{
    std::cout << " Enter the number of experience points: ";
    int Exp;
    std::cin >> Exp;

    std::cout << "\n ----- We consider----- " << std::endl;

    if (Exp > 5000)
    {
        std::cout << " Your level: " << 4 << "\n";
    }
    else if (Exp > 2500)
    {
        std::cout << " Your level: " << 3 << "\n";
    }
    else if (Exp > 1000)
    {
        std::cout << " Your level: " << 2 << "\n";
    }
    else
    {
        std::cout << " Your level: " << 1 << "\n";
    }
}

void GetBarbershopCalculator()
{
    int mansCount;
    int barbersCount;

    std::cout << "************** Barbershop Calculator **************\n";
    std::cout << " Enter the number of men in the city: ";
    std::cin >> mansCount;

    std::cout << " How many barriers have you managed to hire: ";
    std::cin >> barbersCount;
    
    // Сколько человек может постричь один барбер за одну смену?
    int mansPerBarber = 8; // один человек в час, смена 8 часов
    // Сколько человек успеет постричь барбер за месяц?
    int mansPerBarberPerMonth = mansPerBarber * 30;
    std::cout << " One barrier cuts so many customers a month " << mansPerBarberPerMonth << "\n";
    
    if( mansCount % (barbersCount * mansPerBarberPerMonth) == 0 && mansCount / (barbersCount * mansPerBarberPerMonth) == 1 )
    {        
        std::cout << " There are exactly as many barriers as you need!!!\n";
    }
    else if  ( mansCount / (barbersCount * mansPerBarberPerMonth) < 1 )
    {
        std::cout << " There are a lot of barbers!!!\n";
    }
    else
    {
        std::cout << " We need more barbers!!!\n";
    }
}

int main(int argc, char* argv[])
{
    //FindSmallerNum(); //4.4.1

    //FindSumma(); //4.4.2

    //FindOdd(); //4.4.3

    //FindRemainder(); //4.4.5

    //FindMaxSalaryVar1(); //4.4.8
    //FindMaxSalaryVar2(); //4.4.8

    //FindTax(); //4.4.9

    //GetMenu(); //4.4.7

    //GetLevel(); //4.4.4

    //GetBarbershopCalculator();

    
    return 0;
}
